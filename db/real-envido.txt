3
No piense en un descuido…;si no es pa’ tanto la cosa,;yo le digo Real envido;que es lo mesmo que olorosa.
Con su boquita de grana;y su pelo renegrido,;no envidia a la manniana;este hermoso Real envido.
Aqui no he de recular;aunque me vea perdido;cinco puntos vi'a sacar;si aceptan mi real envido.

"Real Envido": [
    "No piense en un\ndescuido\nsi no es p\u00e1tanto\nla cosa...\nyo le digo\nReal Envido\nque es\nlo mesmo que olorosa",
    "Con su boquita\nde grana y su\npelo renegrido\nno envidia\na la manana este\nhermoso Real Envido",
    "Tenes Real Envido ?",
    "Tomala vos\nDamela a m\u00ed\nCon Real Envido\nte vas a divertir...",
    "Si la computadora\nte ha aturdido,\nhabras de perdonarla:Es que vino cargada\ncon un esplendoroso\nReal Envido",
    "Qu\u00e9 coraje...\nQu\u00e9 atrevido andar\ncomiendo electrones\ny escupiendo\nReal Envido",
    "Aunque suene\npresumido yo te cantoel Real Envido",
    "Un gaucho baj\u00f3\ndel cielo\nen un plato volador,\nal pasar junto\na una vaca,\nReal Envido le grit\u00f3",
    "Me llenaste de broncaAhora el partido\nte lo gano\ncon Real Envido\ny con toda pompa",
    "Che, humanoide:\nQuedaste tan\nresentido que hace\nmucho que no me\ngritas el Real Envido",
    "No s\u00e9 si arriesgarmetanto... No s\u00e9 si\ntengo que tirarme\nel lance...\ny Bu\u00e9\nte lo digo nomas:\nReal Envido !!"
  ],
