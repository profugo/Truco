'
'   Recursos libres usados
'
'   Baraja: URL - https://es.m.wikipedia.org/wiki/Archivo:Baraja_espa%C3%B1ola_completa.png
'           LIC - CC by SA 3.0 DEED - https://creativecommons.org/licenses/by-sa/3.0/deed.es
'
'   Musica: URL - https://www.listencopyleftonward.com/downloads/ TEMA - Winternight.mid
'           LIC - CC BY SA 4.0 DEED - https://creativecommons.org/licenses/by-sa/4.0/deed.es
'
$Unstable:Midi
$MidiSoundFont:Default

Option _Explicit

Const VERDE_CLARO& = _RGB32(0, 64, 0)
Const VERDE_OSCURO& = _RGB32(0, 32, 0)
Const AMARILLO& = _RGB32(255, 255, 90)
Const BLANCO& = _RGB32(255, 255, 255)
Const ROJO& = _RGB32(255, 0, 0)
Const NEGRO& = _RGB32(0, 0, 0)

Const strNombre = "Truco Uruguayo DARKNOID"
Const strVersion = "0.1a Build 040417.1-240806.1"

Type tJugador
    Carta As String * 6 '   Pares palo,valor 0-3 palo, 0-9 valor
    Jugada As String * 3 '  Cartas ya jugadas
    Valor As String * 6 '   3 valores decimales de dos caracteres, valores para canto y toque
    Peso As String * 8 '    Ponderacion de cada carta para truco
    Ordenado As String * 3 'Cartas ordenadas por ponderacion de menor a mayor
    TantosDeFlor As Integer
    TantosDeEnvido As Integer
    Flor As Integer
    Palabra As String '     Secuencia de cantos, toques y gritos dados.
    '                           c: canto
    '                               d: quiero
    '                               e: contraflor
    '                               f: flor
    '                           t: toque
    '                               u: envido
    '                               v: hasta igualar
    '                               w: falta envido
    '                           g: grito
    '                               h: quiero
    '                               i: retruco
    '                               j: vale 4
End Type

Type tJuego
    Jugador As tJugador '   Valores del jugador
    IA As tJugador '        Valores de la IA
    Muestra As Integer '    Carta usada de muestra en la vuelta
    Mano As Integer '       0 / 1 :IA o Jugador, quien abre
    Turno As Integer '      0 / 1: Juega IA o Jugador
    Tiradas As Integer '    Manos ganadas, decenas = IA, unidades = Jugador

    cJugador As String * 5 'Carta jugada por el humano
    cIA As String * 4 '     Carta jugada por la IA
    TantosEl As Integer '   Puntos de la IA
    TantosYo As Integer '   Puntos del Jugador
    EnJuego As Integer '    Tantos de truco en juego en la tirada
    Palabra As Integer '    -1: Nadie, 0:Mano, 1:Pie
    Voces As String '       Secuencia de cantos, toques y gritos.
    '                           c: canto
    '                               d: quiero
    '                               e: contraflor
    '                               f: flor
    '                           t: toque
    '                               u: envido
    '                               v: hasta igualar
    '                               w: falta envido
    '                           g: grito
    '                               h: quiero
    '                               i: retruco
    '                               j: vale 4
    TantosEnJuego As Integer
    Partida As Integer '    Tantos de partido 30 o 40
    Terminar As Integer '   0:Mo terminar
    '                       1: terminar la partida
    '                       2:Tirada ganada por IA
    '                       3: Tirada ganada por jugador
    '                       4: Alguien gano el partido
    '                       5: IA Gana mano
    '                       6: Jugador gana mano
    Comando As String '     Ultimo comando ejecutado
End Type

Dim Shared bgm As Long, canto As Long

Dim Shared Carta As Long, Lomo As Long, Una As Long, Rotada As Long, Muestra As Long, Mazo As Long
Dim Shared imgMenu As Long, imgMazo As Long, bufAsist As Long
Dim Shared centroXi As Long, centroYi As Long, centroXf As Long, centroYf As Long, cMax As Single

Dim Shared Lado As Integer, iLado As Long, icono&, iOfs As Integer, ovx As Long, ovy As Long, rel As Single

Dim Shared grados As Integer, paso As Single, i As Single, tOfs As Single, xf As Single, yf As Single, yf2 As Single
Dim Shared saltoX As Single, saltoY As Single, cx As Single, cy As Single, icx As Single, icy As Single
Dim Shared uvx As Single, cxr As Single, cyr As Single, paloMuestra As Integer, valorMuestra As Integer
Dim Shared a$, vx As Single, vy As Single

Dim Shared doBgm As Integer, doSonido As Integer, doChina As Integer, comandos As String, doGaucho As Integer
Dim Shared Baraja(39) As Integer, iBaraja As Integer, Juego As tJuego
Dim Shared man, mal, mr, dm, an, al, usar_asistente

bgm = _SndOpen("snd/TangoLa_Cumparsita.mid")
canto = _SndOpen("snd/TangoA_Media_Luz.mid")

' ReadINI es una funcion mia que sirve para leer tuplas de un archivo de texto, devuelve un string
'
' Formato:
'
' ReadINI(<Archivo>, <Grupo>, <Clave>)
doBgm = Val(ReadINI("settings.ini", "Truco", "BGM"))
If doBgm = 1 Then _SndLoop bgm
doSonido = Val(ReadINI("settings.ini", "Truco", "SONIDO"))
doChina = Val(ReadINI("settings.ini", "Truco", "SUGERENCIAS"))
doGaucho = Val(ReadINI("settings.ini", "Truco", "VERSOS"))

Inicializar

PCopy _Display, 6 ' Guarda la mesa vacia en el sexto buffer

' --------------------------------------------------------------------------------
' --                    B U C L E   P R I N C I P A L                           --
' --------------------------------------------------------------------------------

While Juego.Terminar <> 1

    PCopy 6, _Display ' Primero limpiar la mesa

    comandos = "r123qngtcmx" + Chr$(27)
    Juego.Terminar = 0
    Juego.Tiradas = 0
    Juego.TantosEnJuego = 0
    Juego.EnJuego = 1
    Juego.Voces = ""
    Juego.IA.Palabra = ""
    Juego.Jugador.Palabra = ""
    LimpiarCentro

    Barajar
    PonMuestra
    DibujaTantos

    'PCopy _Display, 2 ' Guarda la mesa vacia pero con muestra en el segundo buffer
    PCopy _Display, 1 ' Guarda la mesa vacia pero con muestra en el segundo buffer

    PrepararMesa

    Calculos

    DebugMsg

    PrepararMenu

    Dim s$, t$

    If doChina = 1 Then
        s$ = ""
        If Juego.Jugador.TantosDeFlor > 0 Then s$ = "CANTA;" + Trim(Str$(Juego.Jugador.TantosDeFlor)) Else s$ = "TOCA;" + Trim(Str$(Juego.Jugador.TantosDeEnvido))
        LaChina s$ + Mid$(Juego.Jugador.Peso, 7)
        Sleep 2
    End If

    JugarPrimera

    ActualizarMenu

    If Juego.Terminar = 0 Then
        LimpiarCentro
        'Print "163 >"; comandos
        DibujaTantos
        ActualizarMenu
        If Juego.Terminar = 0 Then
            JugarSegunda
            LimpiarCentro
            DibujaTantos
        End If
        ActualizarMenu
        If Juego.Terminar = 0 Then JugarTercera
        ' Algebra booleana, si juego.Mano es 0 lo pone a 1 si no lo pone a 0
        'Juego.Mano = -(Juego.Mano = 0)
    End If

    If Juego.Terminar = 5 Then
        If Juego.TantosEnJuego = 0 Then
            Juego.TantosEl = Juego.TantosEl + 1
        Else
            Juego.TantosEl = Juego.TantosEl + Juego.TantosEnJuego
        End If
    End If

    If Juego.Terminar = 6 Then
        If Juego.TantosEnJuego = 0 Then
            Juego.TantosYo = Juego.TantosYo + 1
        Else
            Juego.TantosYo = Juego.TantosYo + Juego.TantosEnJuego + Juego.EnJuego
        End If
        CargaVerso "mazo", "ME VOY"
    End If

    If Juego.Terminar > 0 Then
        Print Juego.Terminar; Juego.TantosYo: Beep: _Display: a$ = Input$(1)
        DibujaTantos
        Juego.Mano = -(Juego.Mano = 0)
        If Juego.TantosEl >= Juego.Partida Then
            CargaVerso "ia", "JUEGO TERMINADO"
            Juego.Terminar = 4
        Else If Juego.TantosYo >= Juego.Partida Then
                CargaVerso "humano", "JUEGO TERMINADO"
                Juego.Terminar = 4
            End If
        End If
        If Juego.Terminar = 4 Then
            CerarPartida
            If doBgm = 1 Then _SndPlay bgm
            If doSonido = 1 Then _SndStop canto
        End If

        If Juego.Terminar = 5 Then

            Print "Te fuiste al mazo"
        End If

        If Trim(Juego.IA.Palabra) <> "" Then
            Beep
            'Print "Juego.IA.Palabra: "; Juego.IA.Palabra: _Display: Beep: a$ = Input$(1)
            ' c: canto
            '   d: quiero
            '   e: contraflor
            '   f: flor
            ' t: toque
            '   u: envido
            '   v: hasta igualar
            '   w: falta envido
            ' g: grito
            '   h: quiero
            '   i: retruco
            '   j: vale 4
            s$ = "": t$ = ""
            For i = 1 To 4
                If InStr(Juego.IA.Palabra, Mid$("tuvw", i, 1)) > 0 Then
                    s$ = s$ + "; ;Perdoneme si le pido;me conteste el envido"
                    Print "Hay un toque pendiente"
                    t$ = "t"
                End If
                If InStr(Juego.IA.Palabra, Mid$("cdef", i, 1)) > 0 And Juego.Jugador.TantosDeFlor > 0 Then
                    s$ = s$ + "; ;Para el frio un manto;antes, responda al canto"
                    Print "Cante, no tiene nada que decir?"
                    t$ = "c"
                End If
                If InStr(Juego.IA.Palabra, Mid$("gij ", i, 1)) > 0 Then
                    s$ = s$ + "; ;Los antiguos tienen mitos;y uste tiene un grito"
                    If InStr(Juego.IA.Palabra, "i") > 0 Then s$ = s$ + "; ;Comi fideos con tuco;y dije Quiero, retruco"
                    Print "Antes de irse conteste el grito"
                    t$ = "g"
                End If
            Next

            If s$ <> "" Then
                Mensaje "UN MOMENTO", "Mi china va de morado;y no me ha contestado" + s$
                ActualizarMenu
                If InStr(comandos, "g") > 0 Then Toggle comandos, "g"
                If InStr(comandos, "1") > 0 Then Toggle comandos, "1"
                If InStr(comandos, "2") > 0 Then Toggle comandos, "2"
                If InStr(comandos, "3") > 0 Then Toggle comandos, "3"
                If InStr(comandos, "x") > 0 Then Toggle comandos, "x"
                ProcesaEntrada
                If t$ = "t" Then
                    If Juego.TantosEnJuego > 0 Then
                        If Juego.Voces = "n" Then
                            Juego.TantosEl = Juego.TantosEl + Juego.TantosEnJuego + Juego.EnJuego
                        Else
                            If Juego.IA.TantosDeEnvido > Juego.Jugador.TantosDeEnvido Then
                                Juego.TantosEl = Juego.TantosEl + Juego.TantosEnJuego + Juego.EnJuego
                                Juego.TantosEl = Juego.TantosEl + 1
                                Mensaje "TOQUE", "Gane con " + Str$(Juego.IA.TantosDeEnvido)
                            Else If Juego.Jugador.TantosDeEnvido > Juego.IA.TantosDeEnvido Then
                                    Juego.TantosYo = Juego.TantosYo + Juego.TantosEnJuego + Juego.EnJuego
                                    Juego.TantosYo = Juego.TantosYo + 1
                                    Mensaje "TOQUE", "Son buenas"
                                End If
                            End If
                        End If
                    End If

                End If
                'Print t$; " "; Juego.Voces; Juego.TantosEnJuego; Juego.IA.TantosDeEnvido; Juego.Jugador.TantosDeEnvido: _Display: a$ = Input$(1)
            End If

        End If
    End If
Wend

' --------------------------------------------------------------------------------

System

' --------------------------------------------------------------------------------

Sub RotateImage (Degree As Single, InImg As Long, OutImg As Long)
    '** Rotates an image by the degree specified.
    'NOTE: OutImg passed by reference is altered
    'Degree - amount of rotation to add to image (-359.999.. - +359.999...) (INPUT)
    'InImg  - image to rotate (INPUT )
    'OutImg - rotated image   (OUTPUT)
    '** This subroutine based on code provided by Rob (Galleon) on the QB64.NET website in 2009.
    '** Special thanks to Luke for explaining the matrix rotation formula used in this routine.

    Dim px(3) As Integer '     x vector values of four corners of image
    Dim py(3) As Integer '     y vector values of four corners of image
    Dim Left As Integer '      left-most value seen when calculating rotated image size
    Dim Right As Integer '     right-most value seen when calculating rotated image size
    Dim Top As Integer '       top-most value seen when calculating rotated image size
    Dim Bottom As Integer '    bottom-most value seen when calculating rotated image size
    Dim RotWidth As Integer '  width of rotated image
    Dim RotHeight As Integer ' height of rotated image
    Dim WInImg As Integer '    width of original image
    Dim HInImg As Integer '    height of original image
    Dim Xoffset As Integer '   offsets used to move (0,0) back to upper left corner of image
    Dim Yoffset As Integer
    Dim COSr As Single '       cosine of radian calculation for matrix rotation
    Dim SINr As Single '       sine of radian calculation for matrix rotation
    Dim x As Single '          new x vector of rotated point
    Dim y As Single '          new y vector of rotated point
    Dim v As Integer '         vector counter

    If OutImg Then _FreeImage OutImg '              free any existing image
    WInImg = _Width(InImg) '                        width of original image
    HInImg = _Height(InImg) '                       height of original image
    px(0) = -WInImg / 2 '                                                  -x,-y ------------------- x,-y
    py(0) = -HInImg / 2 '             Create points around (0,0)     px(0),py(0) |                 | px(3),py(3)
    px(1) = px(0) '                   that match the size of the                 |                 |
    py(1) = HInImg / 2 '              original image. This                       |        .        |
    px(2) = WInImg / 2 '              creates four vector                        |       0,0       |
    py(2) = py(1) '                   quantities to work with.                   |                 |
    px(3) = px(2) '                                                  px(1),py(1) |                 | px(2),py(2)
    py(3) = py(0) '                                                         -x,y ------------------- x,y
    SINr = Sin(-Degree / 57.2957795131) '           sine and cosine calculation for rotation matrix below
    COSr = Cos(-Degree / 57.2957795131) '           degree converted to radian, -Degree for clockwise rotation
    Do '                                            cycle through vectors
        x = px(v) * COSr + SINr * py(v) '           perform 2D rotation matrix on vector
        y = py(v) * COSr - px(v) * SINr '           https://en.wikipedia.org/wiki/Rotation_matrix
        px(v) = x '                                 save new x vector
        py(v) = y '                                 save new y vector
        If px(v) < Left Then Left = px(v) '         keep track of new rotated image size
        If px(v) > Right Then Right = px(v)
        If py(v) < Top Then Top = py(v)
        If py(v) > Bottom Then Bottom = py(v)
        v = v + 1 '                                 increment vector counter
    Loop Until v = 4 '                              leave when all vectors processed
    RotWidth = Right - Left + 1 '                   calculate width of rotated image
    RotHeight = Bottom - Top + 1 '                  calculate height of rotated image
    Xoffset = RotWidth \ 2 '                        place (0,0) in upper left corner of rotated image
    Yoffset = RotHeight \ 2
    v = 0 '                                         reset corner counter
    Do '                                            cycle through rotated image coordinates
        px(v) = px(v) + Xoffset '                   move image coordinates so (0,0) in upper left corner
        py(v) = py(v) + Yoffset
        v = v + 1 '                                 increment corner counter
    Loop Until v = 4 '                              leave when all four corners of image moved
    OutImg = _NewImage(RotWidth, RotHeight, 32) '   create rotated image canvas
    '                                               map triangles onto new rotated image canvas
    _MAPTRIANGLE (0, 0)-(0, HInImg - 1)-(WInImg - 1, HInImg - 1), InImg TO _
                 (px(0), py(0))-(px(1), py(1))-(px(2), py(2)), OutImg
    _MAPTRIANGLE (0, 0)-(WInImg - 1, 0)-(WInImg - 1, HInImg - 1), InImg TO _
                 (px(0), py(0))-(px(3), py(3))-(px(2), py(2)), OutImg

End Sub

' --------------------------------------------------------------------------------

Sub CerarPartida ()
    Juego.Mano = 0
    Juego.Turno = 1
    Juego.TantosEl = 0
    Juego.TantosYo = 0
    Juego.cJugador = ""
    Juego.cIA = ""
    Juego.Palabra = -1
    Juego.TantosEnJuego = 0
    Juego.EnJuego = 1
    Juego.Terminar = 0
End Sub

' --------------------------------------------------------------------------------

Sub Inicializar ()
    Dim a As Integer, b As Integer

    CerarPartida
    Juego.Partida = Val(ReadINI("settings.ini", "Truco", "TANTOS"))
    If Juego.Partida <> 40 Then Juego.Partida = 30

    imgMenu = _LoadImage("img/c-menu.png")
    imgMazo = _LoadImage("img/c-mazo.png")
    Mazo = _LoadImage("Cartas/mazo.png")
    ovx = 208 ' Ancho de una carta
    ovy = 319 ' Alto de una carta

    Carta = _NewImage(ovx, ovy, 32)
    Lomo = _NewImage(ovx, ovy, 32)

    TomaCarta 5, 2, Mazo, Lomo

    Lado = (-_DesktopHeight * (_DesktopWidth > _DesktopHeight) - _DesktopWidth * (_DesktopHeight >= _DesktopWidth)) * .85
    _Title strNombre + " v" + strVersion

    ' En Linux no anda lo del icono
    icono& = _LoadImage("Cartas/icon.svg")
    If icono& < -1 Then _Icon icono&
    _FreeImage icono&

    Screen _NewImage(Lado, Lado, 32)
    _ScreenMove _Middle

    ' En _FullScreen da un error "40 (X_TranslateCoords)", sin complicarme, voy a ventana flotante

    Randomize Timer

    iLado = Lado * .95
    iOfs = (Lado - iLado) \ 2

    rel = ovx / ovy
    ovy = Lado \ 5 '        Toma la relacion de proporcion de una carta y la escala
    ovx = Int(ovy * rel) '  para que cada carta tenga una altura de 1/5 del alto de
    '                       pantalla, haciendo un escalado adaptativo independiente
    '                       de la resolucion del host.

    centroXi = Lado \ 2: centroYi = centroXi

    cMax = ovy * 1.3

    grados = 360
    paso = 15

    tOfs = (Lado - (ovx * 3.9)) \ 2 ' Centrado del resaltado amarillo de cartas del jugador

    yf = Lado - (Lado - iLado) - cMax
    yf2 = Lado - iLado

    Line (0, 0)-Step(Lado, Lado), VERDE_CLARO&, BF
    Line (iOfs, iOfs)-Step(iLado, iLado), VERDE_OSCURO&, BF

    icx = centroXi + ovx * 1.5 + ovx
    icy = centroYi - (ovy / 2)

    For a = 0 To 4
        _PutImage (icx, icy)-Step(ovx, ovy), Lomo
        icx = icx + 2: icy = icy + 2
    Next

    an = ovx * 1.3
    al = ovy * 1.3

    For b = 0 To 2
        Line (tOfs + b * an, yf)-Step(an, al), AMARILLO&, B
        Line (tOfs + b * an, yf2)-Step(an, al), AMARILLO&, B
    Next

    mr = _Width(imgMenu) / _Height(imgMenu)
    man = tOfs * .95
    mal = man / mr
    dm = (tOfs - man) / 2
    _PutImage (dm, yf)-Step(man, mal), imgMenu, _Smooth
    man = man / 3

    _PutImage (Lado - tOfs + man, yf + man)-Step(man, man), imgMazo, _Smooth

    PCopy _Display, 1

End Sub

' --------------------------------------------------------------------------------

Function ValorPalo (valor)
    ValorPalo = Floor(valor / 10)
End Function

' --------------------------------------------------------------------------------

Function ValorCarta (valor)
    Dim c
    c = Floor(valor / 10)
    ValorCarta = valor - c * 10
End Function

' --------------------------------------------------------------------------------

Sub TomaCarta (palo As Integer, valor As Integer, Fuente As Long, Imagen As Long)
    Dim x, y
    If palo = -1 Then
        palo = 5
        valor = 2
    End If

    x = (valor - 1) * 208
    y = (palo - 1) * 319
    _PutImage (0, 0), Fuente, Imagen, (x, y)-Step(207, 318)
End Sub

' --------------------------------------------------------------------------------

Sub PonMuestra
    Dim palo As Integer, valor As Integer
    Dim fcx, fcy, a

    icx = centroXi + ovx * 1.5 + ovx
    icy = centroYi - (ovy / 2)

    palo = Floor(Juego.Muestra / 10) + 1
    valor = Juego.Muestra - ((palo - 1) * 10) + 1

    TomaCarta palo, valor, Mazo, Carta

    fcx = centroXi + ovx * 1.5 + ovx * .75
    fcy = centroYi - (ovy / 2) - ovy \ 6

    _PutImage (fcx, fcy)-Step(ovx, ovy), Carta

    For a = 0 To 4
        _PutImage (icx, icy)-Step(ovx, ovy), Lomo
        icx = icx + 2: icy = icy + 2
    Next

    _Display: PCopy _Display, 1
End Sub

' --------------------------------------------------------------------------------

Sub DaCarta (palo As Integer, valor As Integer, posicion As Integer)
    Dim a, vx As Single, vy As Single

    TomaCarta palo, valor, Mazo, Carta
    Dim an As Single, al As Single
    an = ovx * 1.3
    al = ovy * 1.3

    xf = tOfs + an * posicion
    vx = ovx
    vy = ovy

    centroXf = an / 2 + xf
    centroYf = al / 2 + yf

    saltoX = ((centroXf - centroXi) / (grados \ (paso - 1)))
    saltoY = ((centroYf - centroYi) / (grados \ (paso - 1)))

    cx = centroXi
    cy = centroYi

    For a = 0 To grados Step paso
        _Limit 60

        uvx = vx - (vx < 1)
        Una = _NewImage(uvx, vy)
        If i < 0 Then Muestra = Lomo Else Muestra = Carta
        _PutImage (0, 0)-(vx, vy), Muestra, Una
        RotateImage a, Una, Rotada
        _FreeImage (Una)
        vx = vx + i
        If vx <= 1 Then i = -i
        If vx > ovx Then vx = ovx: i = 0

        cx = cx + saltoX
        cy = cy + saltoY

        cxr = _Width(Rotada) \ 2
        cyr = _Height(Rotada) \ 2

        PCopy 1, _Display

        _PutImage (cx - cxr, cy - cyr), Rotada

        _Display
    Next

End Sub

' --------------------------------------------------------------------------------

Sub JuegaCarta (palo As Integer, valor As Integer, posicion As Integer)
    Dim a, vx As Single, vy As Single, dif, pyf, pxf

    dif = 0
    centroXi = Lado \ 2: centroYi = centroXi

    If posicion > 9 Then
        dif = -32
        posicion = posicion - 10
        Line (tOfs + posicion * an + 4, yf2 + 4)-Step(an - 8, al - 8), VERDE_OSCURO, BF
    Else
        Line (tOfs + posicion * an + 4, yf + 4)-Step(an - 8, al - 8), VERDE_OSCURO, BF
    End If

    TomaCarta palo, valor, Mazo, Carta
    Dim an As Single, al As Single
    an = ovx * 1.3
    al = ovy * 1.3

    pyf = yf
    pxf = xf
    xf = tOfs + an * posicion
    yf = yf
    vx = ovx
    vy = ovy

    centroXi = centroXi + dif
    centroYi = centroYi + dif
    centroXf = an / 2 + xf
    centroYf = al / 2 + yf

    Swap centroXf, centroXi
    Swap centroYf, centroYi

    PCopy _Display, 1

    saltoX = ((centroXf - centroXi) / (grados \ (paso - 1)))
    saltoY = ((centroYf - centroYi) / (grados \ (paso - 1)))

    cx = centroXi
    cy = centroYi

    For a = 0 To grados Step paso
        _Limit 60

        uvx = vx - (vx < 1)
        Una = _NewImage(uvx, vy)
        If i < 0 Then Muestra = Lomo Else Muestra = Carta
        _PutImage (0, 0)-(vx, vy), Muestra, Una
        RotateImage a, Una, Rotada
        _FreeImage (Una)
        vx = vx + i
        If vx <= 1 Then i = -i
        If vx > ovx Then vx = ovx: i = 0

        cx = cx + saltoX
        cy = cy + saltoY

        cxr = _Width(Rotada) \ 2
        cyr = _Height(Rotada) \ 2

        PCopy 1, _Display

        _PutImage (cx - cxr, cy - cyr), Rotada

        _Display
    Next

    Swap centroXf, centroXi
    Swap centroYf, centroYi
End Sub

' --------------------------------------------------------------------------------

Sub PreparaCarta ()
    Dim a
    icx = centroXi + ovx * 1.5 + ovx + 10
    icy = centroYi - (ovy / 2) + 10
    saltoX = (icx - (centroXi - (ovx / 2))) / 29
    saltoY = .333 '  10 / 30

    For a = 0 To 29
        _Limit 60
        PCopy 1, _Display
        _PutImage (icx, icy)-Step(ovx, ovy), Lomo
        icx = icx - saltoX
        icy = icy - saltoY

        _Display
    Next a
    PCopy 1, _Display
    _PutImage (centroXi - ovx \ 2, centroYi - ovy \ 2)-Step(ovx, ovy), Lomo

    _Delay .125

End Sub

' --------------------------------------------------------------------------------

Sub Barajar
    Dim a As Integer, b As Integer, i As Integer

    For a = 0 To 39
        Baraja(a) = a
    Next a

    For a = 0 To 999
        For b = 0 To 39
            i = -1
            While i = b Or i = -1
                i = Int(Rnd * 40)
            Wend
            Swap Baraja(i), Baraja(b)
    Next b, a

    'Baraja(0) = 16
    'Baraja(1) = 15
    'Baraja(2) = 17
    'Baraja(3) = 12
    'Baraja(4) = 22
    'Baraja(5) = 25
    'Baraja(6) = 39

    Juego.Muestra = Baraja(0)
    iBaraja = 1
End Sub

' --------------------------------------------------------------------------------

Function Trim$ (Cadena As String)
    Trim = LTrim$(RTrim$(Cadena))
End Function

' --------------------------------------------------------------------------------

Function Floor% (V)
    Dim f, g

    f = Int(V * 10)
    g = f Mod 10
    Floor% = (f - g) \ 10
End Function

' --------------------------------------------------------------------------------

Sub TantosDeCartas (Pl As tJugador) ' Pl As String, Ret As String)
    Dim intPaloCarta, intValorCarta, intValor, i, c, valor, r$, s$

    s$ = ""

    ' Computa el valor de cada carta por separado
    For i = 0 To 2
        c = Val(Mid$(Pl.Carta, i * 2 + 1, 2))

        valor = 0
        intPaloCarta = ValorPalo(c)
        intValorCarta = ValorCarta(c)

        If paloMuestra = intPaloCarta Then
            Select Case intValorCarta
                Case Is = 0
                    intValor = 1
                Case Is = 1
                    intValor = 30
                Case Is = 2
                    intValor = 3
                Case Is = 3
                    intValor = 29
                Case Is = 4
                    intValor = 28
                Case Is = 5
                    intValor = 6
                Case Is = 6
                    intValor = 7
                Case Is = 7
                    intValor = 27
                Case Is = 8
                    intValor = 27
                Case Is = 9
                    Select Case valorMuestra
                        Case Is = 1
                            intValor = 30
                        Case Is = 3
                            intValor = 29
                        Case Is = 4
                            intValor = 28
                        Case Is = 7
                            intValor = 27
                        Case Is = 8
                            intValor = 27
                        Case Else
                            intValor = 0
                    End Select
            End Select
        Else
            If intValorCarta < 7 Then intValor = intValorCarta + 1 Else intValor = 0
        End If

        r$ = LTrim$(Str$(intValor))
        If Len(r$) < 2 Then r$ = "0" + r$
        s$ = s$ + r$
    Next

    Pl.Valor = s$
End Sub

' --------------------------------------------------------------------------------
Function TantosConPieza (Valores As String, Modo As Integer)
    Dim intTantos, i, v(3), j

    j = (Modo And &HF0) / 16
    Modo = Modo And 7
    'Print "j,modo"; j; Modo

    'Busca la carta de mayor valor en busca de piezas
    For i = 0 To 2
        v(i) = Val(Mid$(Valores, i * 2 + 1, 2))
    Next

    If j > 0 Then
        j = -1
        intTantos = 0
        For i = 0 To 2
            If v(i) > intTantos Then intTantos = v(i): j = i
        Next
        For i = 0 To 2
            If j <> i Then v(i) = ValorCarta(v(i))
        Next
    End If
    TantosConPieza = v(0) + v(1) + v(2)
End Function

' --------------------------------------------------------------------------------

Function EsFlor (j As tJugador)
    Dim p(3), v(3), i, a, t

    For i = 0 To 2
        p(i) = ValorPalo(Val(Mid$(j.Carta, i * 2 + 1, 2)))
        v(i) = Val(Mid$(j.Valor, i * 2 + 1, 2))
    Next i

    a = 0
    If p(0) = p(1) Then a = 1: t = 2
    If p(0) = p(2) Then a = a + 2: t = 1
    If p(1) = p(2) Then a = a + 4: t = 0

    If a < 7 Then
        Select Case a
            Case Is = 1
                If v(0) + v(1) > 40 Or v(t) > 20 Then a = 17
            Case Is = 2
                If v(0) + v(2) > 40 Or v(t) > 20 Then a = 18
            Case Is = 4
                If v(1) + v(2) > 40 Or v(t) > 20 Then a = 20
        End Select
    End If

    EsFlor = a
End Function

' --------------------------------------------------------------------------------

Sub SumaTantos (j As tJugador)

    Dim a, intMayor, intTantos, intEnvite, flor, p(3), v(3), c, va

    ' Guarda el palo y el valor de la muestra
    paloMuestra = ValorPalo(Juego.Muestra)
    valorMuestra = ValorCarta(Juego.Muestra)

    TantosDeCartas j ' Calcular el valor de cada carta del jugador j
    intMayor = 0
    For a = 0 To 2
        p(a) = ValorPalo(Val(Mid$(j.Carta, a * 2 + 1, 2)))
        v(a) = Val(Mid$(j.Valor, a * 2 + 1, 2))
        If v(a) > v(intMayor) Then intMayor = a
    Next

    flor = EsFlor(j) ' Luego se fija si hay flor
    '   Valores
    '   0: no hay
    '   1: primera y segunda del mismo palo
    '   2: segunda y tercera del mismo palo
    '   4: primera y tercera del mismo palo

    If flor > 0 Then

        If flor = 7 Then
            If p(0) = paloMuestra Then
                va = 0
                c = -1
                For a = 0 To 2
                    If v(a) > va Then va = v(a): c = a
                Next
                intTantos = v(c)
                Select Case c
                    Case Is = 0
                        intTantos = intTantos + ValorCarta(v(1)) + ValorCarta(v(2))
                    Case Is = 1
                        intTantos = intTantos + ValorCarta(v(0)) + ValorCarta(v(2))
                    Case Is = 2
                        intTantos = intTantos + ValorCarta(v(0)) + ValorCarta(v(1))
                End Select
                If v(c) < 20 Then intTantos = v(0) + v(1) + v(2) + 20
            Else
                intTantos = v(0) + v(1) + v(2) + 20
            End If
        Else
            If flor > 15 Then
                intTantos = TantosConPieza(j.Valor, flor) ' Calcula los tantos siempre que tenga una pieza
            Else

                If flor > 0 And flor < 7 Then
                    intEnvite = 0
                    Select Case flor
                        Case Is = 1
                            intEnvite = v(0) + v(1)
                        Case Is = 2
                            intEnvite = v(0) + v(2)
                        Case Is = 4
                            intEnvite = v(2) + v(1)
                    End Select
                    If intEnvite < 20 Then intEnvite = intEnvite + 20
                    'Print "Envido con dos del mismo palo:"; intEnvite
                End If
            End If
        End If
    Else
        intEnvite = 0
        If (intTantos = 0) Then ' No hay flor, Envido?
            'Print "calc env"
            If flor = 0 Then
                For a = 0 To 2
                    If v(2) > v(0) Then Swap v(0), v(2)
                    If v(1) > v(0) Then Swap v(0), v(1)
                    If v(2) > v(1) Then Swap v(1), v(2)
                Next
                v(1) = ValorCarta(v(1))
                intEnvite = v(0) + v(1) - 20 * (v(0) < 20)
                'Print "e:"; intEnvite
            End If
        End If

    End If
    '_Display

    j.Flor = flor
    j.TantosDeFlor = intTantos
    j.TantosDeEnvido = intEnvite
End Sub

' --------------------------------------------------------------------------------

Sub Mensaje (titulo As String, versos As String)
    Dim i As Long, f As Long, m As Long, an, al, r, a, b, ytxt As Integer, otxt As String
    Dim linea As Integer, s1 As Integer, s2 As Integer, txt As String, cadena As String

    ytxt = 5
    linea = 24
    an = 590 ' Ancho
    al = 275 ' Alto
    r = an / al ' Relacion de aspecto

    If doBgm = 1 Then Sleep 1: _SndPause bgm
    If doSonido = 1 Then _SndPlay canto

    titulo = " " + titulo + " "
    i = (linea - Len(titulo)) / 2
    f = Floor(i)
    titulo = String$(f, "-") + titulo + String$(f, "-")
    If Len(titulo) < linea Then titulo = titulo + "-"
    If Len(titulo) > linea Then titulo = Mid$(titulo, 2, linea)

    i = _LoadImage("img/Los_gauchos.png")
    'i = _LoadImage("img/china.png")
    m = _NewImage(an, al, 32)
    f = _LoadFont("Font/nk57-monospace-se-sb.otf", 12)
    _Dest m
    Line (0, 0)-(an, al), _RGBA(0, 0, 32, 220), BF
    _PutImage (15, 15)-(335, 255), i
    Color BLANCO, 0
    _Font f
    Locate 3, 360
    Print titulo
    Locate 1, 1
    s1 = 0

    Do
        b = InStr(1, versos, ";")
        If b > 1 Then
            txt = Mid$(versos, 1, b - 1)
            versos = Mid$(versos, b + 1)
        Else
            txt = versos
            versos = ""
            s1 = 1
        End If
        s2 = 0
        Do
            cadena = Trim(Mid$(txt, 1, linea))
            otxt = txt

            a = linea
            If Mid$(txt, linea + 1, 1) <> " " And Len(txt) > linea Then
                While Mid$(cadena, a, 1) <> " ": a = a - 1: Wend
                cadena = Trim(Mid$(txt, 1, a))
                txt = Trim(Mid$(txt, a + 1))
            Else
                If Len(txt) <= linea Then
                    txt = ""
                    s2 = 1
                Else
                    txt = Trim(Mid$(txt, linea + 1))
                    If txt = "" Then s2 = 1
                    If Len(txt) < linea Then txt = txt + String$(linea, " ")
                End If
            End If
            If Len(txt) <= linea Then txt = txt + String$(linea, " ")
            Locate ytxt, 360
            Print cadena;
            ytxt = ytxt + 1
        Loop While s2 = 0
        _Display
    Loop While s1 = 0

    PCopy _Display, 3
    _Dest 0
    an = Lado * .95
    al = an / r
    Line (0, 0)-Step(Lado, Lado), _RGBA(0, 0, 0, 128), BF
    _PutImage ((Lado - an) \ 2, (Lado - al) \ 2)-Step(an, al), m
    _FreeImage i
    _FreeImage m
    _Display

    i = 0

    Do While i = 0
        ' Primero lee al dispositivo apuntador
        Do While _MouseInput
        Loop

        ' Si se hizo clic
        If _MouseButton(1) <> 0 Then
            i = 1
            Do
                f = _MouseInput
            Loop Until Not _MouseButton(1)
        End If

        a$ = InKey$
        If a$ <> "" Then i = 1
    Loop
    If doSonido = 1 Then _SndStop canto
    If doBgm Then _SndPlay bgm
    PCopy 3, _Display
    _Display

End Sub

' --------------------------------------------------------------------------------

Sub Revelar (posicion As Integer)
    Dim palo As Integer, valor As Integer, an, x, y, centroX, a As Integer, dx, c

    an = ovx * 1.3

    x = tOfs + an * posicion + (ovx * .3) / 2
    y = yf2 + (ovy * .3) / 2
    i = -(ovx / 15)
    vx = ovx
    centroX = x + (ovx / 2)
    dx = ovx / 2

    c = Val(Mid$(Juego.IA.Carta, (posicion * 2) + 1, 2))
    palo = ValorPalo(c) + 1
    valor = ValorCarta(c) + 1
    TomaCarta palo, valor, Mazo, Carta

    Line (x, y)-Step(ovx + 1, ovy + 1), VERDE_OSCURO, BF

    PCopy _Display, 3 ' Guarda la vista actual

    For a = 1 To 30
        uvx = vx - (vx < 1)
        Una = _NewImage(uvx, ovy)
        If i < 0 Then Muestra = Lomo Else Muestra = Carta
        _PutImage (0, 0)-(vx, ovy), Muestra, Una
        PCopy 3, _Display
        _PutImage (x + (ovx - vx) / 2, y), Una
        _FreeImage (Una)
        vx = vx + i
        If vx <= 1 Then i = -i
        If vx > ovx Then vx = ovx: i = 0

        _Limit 60
        _Display
    Next
End Sub

' --------------------------------------------------------------------------------

Sub Ponderar (j As tJugador)
    Dim c(3), v(3), a, numero, numeroCarta, r$, s$, f, an(3), p(3)

    s$ = ""
    f = 0

    For a = 0 To 2
        an(a) = a
        c(a) = Val(Mid$(j.Carta, (a * 2) + 1, 2))
        v(a) = Val(Mid$(j.Valor, (a * 2) + 1, 2))
    Next

    For a = 0 To 2
        numeroCarta = ValorCarta(c(a))
        numero = 0
        If v(a) > 20 Then
            Select Case v(a)
                Case Is = 30
                    numero = 19
                Case Is = 29
                    numero = 18
                Case Is = 28
                    numero = 17
                Case Is = 27
                    Select Case numeroCarta
                        Case Is = 8
                            numero = 16
                        Case Is = 7
                            numero = 15
                    End Select
            End Select
        Else
            Select Case c(a)
                Case Is = 20 '   Espadilla
                    numero = 14
                Case Is = 30 '   Bastillo
                    numero = 13
                Case Is = 26 '   7 de Espadas
                    numero = 12
                Case Is = 36 '   7 de Bastos
                    numero = 11
            End Select
        End If
        If numero = 0 Then
            Select Case numeroCarta
                Case Is = 2
                    numero = 10
                Case Is = 1
                    numero = 9
                Case Is = 0
                    numero = 8
                Case Else
                    numero = numeroCarta - 2
            End Select
        End If
        p(a) = numero
        r$ = Trim(Str$(numero))
        If Len(r$) < 2 Then r$ = "0" + r$
        s$ = s$ + r$
        f = f + numero
    Next
    If p(2) < p(0) Then
        Swap p(0), p(2)
        Swap an(0), an(2)
    End If
    If p(1) < p(0) Then
        Swap p(0), p(1)
        Swap an(0), an(1)
    End If
    If p(2) < p(1) Then
        Swap p(1), p(2)
        Swap an(1), an(2)
    End If

    f = Floor(f / 3)
    r$ = Trim(Str$(f))
    If Len(r$) < 2 Then r$ = "0" + r$

    j.Peso = s$ + r$
    j.Ordenado = Trim(Str$(an(0))) + Trim(Str$(an(1))) + Trim(Str$(an(2)))
End Sub

' --------------------------------------------------------------------------------

Sub LaChina (msg As String)

    Dim ico As Long, m As Long, f As Long, g As Long, i, r, ya, dw, dh, ofs, s$

    If usar_asistente = 1 And bufAsist <> 0 Then
        _PutImage (Lado - tOfs, yf), bufAsist
    End If

    f = _LoadFont("Font/nk57-monospace-se-sb.otf", 22)
    g = _LoadFont("Font/nk57-monospace-se-sb.otf", 70)
    r = 340 / 170
    ya = Lado - (Lado - iLado) - cMax
    dw = tOfs * .95
    dh = dw / r
    ofs = (dw - tOfs) + 2

    m = _NewImage(340, 170, 32)
    _Dest m
    Line (0, 0)-(340, 170), _RGBA(0, 0, 32, 220), BF

    _Font f
    ico = _LoadImage("img/china.png")
    _PutImage (10, 10)-(160, 160), ico, , (10, 0)-(75, 85)

    Color BLANCO, 0
    Locate 7, 290
    Print "[X]";

    Locate 2, 170
    Print "-LA CHINA-";

    i = InStr(msg, ";")

    If i > 0 Then
        s$ = Trim(Mid$(msg, 1, i - 1))
        msg = Mid$(msg, i + 1)
    Else
        s$ = msg
        msg = ""
    End If

    Locate 5, 210
    Print s$;

    _Dest 0
    bufAsist = _NewImage(dw + 4, dh * 2.3, 32)
    _PutImage (0, 0), , bufAsist, (Lado - tOfs, ya)-Step(dw + 4, dh * 2.3)
    usar_asistente = 1

    _PutImage (10, 10)-(160, 160), ico, m, (10, 0)-(75, 85)
    _FreeImage ico
    _PutImage (Lado - tOfs + 2, ya)-Step(dw, dh), m
    _Dest m
    Cls

    s$ = Mid$(msg, 3)
    msg = Mid$(msg, 1, 2)
    Color BLANCO, 0
    If Val(msg) > 0 Then
        _Font g, m
        Locate 2, 120
        Print msg
        If Val(s$) > 8 Then
            _Font f, m
            Locate 2, 120
            Print "Grita";
        End If
    Else
        _Font f, m
        Locate 3, 120
        i = InStr(s$, ";")
        If i > 0 Then
            msg = Mid$(s$, i + 1)
            s$ = Mid$(s$, 1, i - 1)
            Print s$;
            Locate 5, 120
            Print msg;
        Else
            Print s$;
        End If

    End If

    _Dest 0

    Line (Lado - tOfs + 2, ya + dh * 1.2)-Step(dw, dh), _RGBA(0, 0, 32, 220), BF
    _PutImage (Lado - tOfs + 2, ya + dh * 1.2)-Step(dw, dh), m
    _Display

End Sub

' --------------------------------------------------------------------------------
Sub NoQN (comandos As String)

    Line (dm, yf)-Step(mal / 3 * 2, mal), _RGBA32(0, 0, 0, 220), BF
    Toggle comandos, "q"
    Toggle comandos, "n"
End Sub

' --------------------------------------------------------------------------------

Sub Toggle (comandos As String, comando As String)
    Dim i As Integer, s$

    i = InStr(comandos, comando)
    If i > 0 Then If Len(comandos) = 1 Then s$ = "" Else s$ = Mid$(comandos, 1, i - 1)
    If i < Len(comandos) Then s$ = s$ + Mid$(comandos, i + 1)
    If i = 0 Then s$ = s$ + comando
    comandos = s$
End Sub

' --------------------------------------------------------------------------------

Sub DibujaPalitos (Segmento, Tantos, ElYo)
    Dim xofs, yofs, l, lim, buenas, colour As Long
    l = 30
    buenas = 30
    colour = NEGRO
    yofs = 65 + (Segmento - 1) * (l + 4)
    xofs = 45
    If Juego.Partida = 40 Then
        l = 25
        yofs = 60 + (Segmento - 1) * (l + 2)
        buenas = 20
    End If
    lim = Floor(Floor(Juego.Partida / 2) / 5)

    If Segmento > lim Then
        colour = ROJO
        yofs = yofs + buenas
    End If
    If ElYo = 0 Then xofs = 140

    If Tantos > 0 Then Line (xofs, yofs)-Step(2, l), colour, BF
    If Tantos > 1 Then Line (xofs, yofs + l - 4)-Step(l, 2), colour, BF
    If Tantos > 2 Then Line (xofs + l - 4, yofs)-Step(2, l), colour, BF
    If Tantos > 3 Then Line (xofs, yofs)-Step(l, 2), colour, BF
    If Tantos > 4 Then
        Line (xofs - 1, yofs + 1)-Step(l - 2, l - 2), colour
        Line -Step(2, -2), colour
        Line -Step(-l + 2, -l + 2), colour
        Paint (xofs + 10, yofs + 10), colour
    End If

End Sub

' --------------------------------------------------------------------------------

Sub DibujaTantos ()
    Dim w, h, g, s, r, i, dtantos As Long

    Juego.cIA = ""
    Juego.cJugador = ""

    w = 208 ' Ancho de una carta
    h = 319 ' Alto de una carta

    dtantos = _NewImage(w, h, 32)

    _Dest dtantos

    g = _LoadFont("Font/nk57-monospace-se-sb.otf", 45)
    _Font g
    Line (0, 0)-Step(w, h), _RGBA(255, 255, 255, 200), BF
    Line (102, 16)-Step(4, 287), NEGRO, BF
    Line (16, 48)-Step(176, 4), _RGB(0, 0, 0), BF
    Line (16, 176)-Step(176, 4), _RGB(0, 0, 0), BF

    Color _RGB(0, 0, 0), 0
    Locate 1, 15
    Print "Yo"
    Locate 1, 120
    Print "Ia"

    s = Floor(Juego.TantosYo / 5) ' Segmentos de 5 completos
    r = Juego.TantosYo Mod 5 ' Resto de 5
    i = 1
    If s > 0 Then
        For i = 1 To s
            DibujaPalitos i, 5, 1
        Next
    End If
    If r > 0 Then DibujaPalitos i, r, 1

    s = Floor(Juego.TantosEl / 5) ' Segmentos de 5 completos
    r = Juego.TantosEl Mod 5 ' Resto de 5
    i = 1
    If s > 0 Then
        For i = 1 To s
            DibujaPalitos i, 5, 0
        Next
    End If
    If r > 0 Then DibujaPalitos i, r, 0

    _Dest 0
    Line (tOfs + an * 3.2, yf2)-Step(an, al), VERDE_OSCURO, BF
    _PutImage (tOfs + an * 3.2, yf2)-Step(an, al), dtantos
    _Display
    _FreeImage dtantos

End Sub

' --------------------------------------------------------------------------------

Sub Asistente ()
    Dim v, cIa, cJugador, tmp, tmp2, resto, jjo As String

    If doChina = 1 Then
        Dim res$(4)
        res$(0) = "primera"
        res$(1) = "segunda"
        res$(2) = "tercera"
        res$(3) = "retirada"

        jjo = Trim(Juego.Jugador.Ordenado)
        v = 1
        tmp = Val(Mid$(jjo, 1, 1))

        If Val(Mid$(Juego.Jugador.Peso, 7)) < 5 And Len(jjo) > 1 Then
            v = 4
        Else
            If Len(Trim(Juego.cIA)) > 0 Then
                cIa = Val(Mid$(Juego.cIA, 3))
                cJugador = Val(Mid$(jjo, v, 1))
                tmp = cJugador * 2 + 1
                While cJugador <= cIa And v <= Len(jjo)
                    cJugador = Val(Mid$(jjo, v, 1))
                    tmp = cJugador * 2 + 1
                    cJugador = Val(Mid$(Juego.Jugador.Peso, tmp, 2))
                    If cJugador <= cIa Then v = v + 1
                Wend
            End If
            If v > Len(jjo) Then v = 1
            tmp = Val(Mid$(jjo, v, 1))
        End If
        If v < 4 Then v = tmp Else v = 3

        If Len(jjo) > 1 And v < 3 Then
            resto = 0
            For tmp = 2 To Len(jjo)
                tmp2 = Val(Mid$(jjo, tmp, 1))
                resto = resto + Val(Mid$(Juego.Jugador.Peso, tmp2 * 2 + 1, 2))
            Next
            resto = Floor(resto / (Len(jjo) - 1))
        Else
            resto = 99
        End If
        If resto < (Len(jjo) * 2) + 2 Then v = 3
        LaChina "SUG.;00Juga la;" + res$(v)
    End If

End Sub

' --------------------------------------------------------------------------------

Sub PrimeraJugador
    If Juego.cJugador <> "" Then Toggle Juego.Jugador.Ordenado, Mid$(Juego.cJugador, 5, 1)
End Sub

' --------------------------------------------------------------------------------

Function MejorJugadaIA ()
    Dim cIa, cJugador, i, resto, mayor, l, peso, s As String

    s = Trim(Juego.IA.Ordenado)
    If s = "" Or Juego.cJugador = "" Then Exit Function
    l = Len(s)

    i = 1
    cIa = PesoCartaIA(i)

    If cIa <= cJugador And l > 0 Then
        i = 2
        cIa = PesoCartaIA(i)
    End If
    If cIa <= cJugador And l > 1 Then
        i = 3
        cIa = PesoCartaIA(i)
    End If
    If cIa <= cJugador Then i = 1

    resto = 0
    cJugador = Val(Mid$(Juego.cJugador, 3, 2))
    cIa = PesoCartaIA(i)
    While i < l And cIa <= cJugador
        i = i + 1
        cIa = PesoCartaIA(i)
    Wend

    If cIa <= cJugador Then
        i = 1
    End If
    mayor = 0
    If l = 1 Then
        s = ""
    Else

        l = i
        For i = 1 To Len(s)
            If i <> l Then
                peso = PesoCartaIA(i)
                If peso > mayor Then mayor = peso
                resto = resto + peso
            End If
        Next

        If Len(s) > 1 Then resto = Floor(resto / (Len(s) - 1))
    End If
    MejorJugadaIA = l * 10000 + resto * 100 + mayor ' Formato ABBCC
    '                                                   A: Carta de mano a jugar 0-2
    '                                                   BB: Valoracion media del resto de cartas
    '                                                   CC: Valoracion de carta mayor
End Function

' --------------------------------------------------------------------------------

Function PesoCartaIA (i)
    PesoCartaIA = Val(Mid$(Juego.IA.Peso, (Val(Mid$(Juego.IA.Ordenado, i, 1))) * 2 + 1, 2))
End Function

' --------------------------------------------------------------------------------

Sub JugarCartaIA (c)
    If InStr(Juego.IA.Ordenado, Trim(Str$(c))) > 0 Then Toggle Juego.IA.Ordenado, Trim(Str$(c))
End Sub

' --------------------------------------------------------------------------------

Sub CantosIA
    If Juego.IA.TantosDeFlor > 0 Then
        If Val(Mid$(Juego.IA.Peso, 7, 2)) > 8 Then
            CargaVerso "flor-truco", "CANTO"
            Juego.Voces = "cg"
            Juego.IA.Palabra = "cg"
        Else
            CargaVerso "flor", "CANTO"
            Juego.Voces = "c"
            Juego.IA.Palabra = "c"
        End If
        Juego.TantosEl = Juego.TantosEl + 3
        Toggle comandos, "t"
        Line (dm + (mal / 3 * 2), yf + mal / 3)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
        DibujaTantos
    Else If Juego.IA.TantosDeEnvido > 27 And InStr(Trim(Juego.Voces), "c") = 0 Then
            CargaVerso "envido", "TOQUE"
            Print Trim(Juego.Voces)
            Juego.Voces = "t"
            Juego.IA.Palabra = "t"
            Juego.TantosEnJuego = 1
        End If
    End If

End Sub

' --------------------------------------------------------------------------------

Sub ActualizarMenu

    Dim voces$, a, c$, bandera

    '   VOCES
    '   c: canto
    '       d: quiero
    '       e: contraflor
    '       f: flor
    '   t: toque
    '       u: envido
    '       v: hasta igualar
    '       w: falta envido
    '   g: grito
    '       h: querido
    '       i: retruco
    '       j: vale 4
    voces$ = "tuvwgij"
    mr = _Width(imgMenu) / _Height(imgMenu)
    man = tOfs * .95
    mal = man / mr
    dm = (tOfs - man) / 2
    _PutImage (dm, yf)-Step(man, mal), imgMenu, _Smooth

    If Juego.Jugador.TantosDeFlor > 0 Or InStr(Juego.Voces, "c") > 0 Or Juego.Tiradas > 0 Then
        If InStr(comandos, "t") > 0 Then Toggle comandos, "t"
        Line (dm + (mal / 3 * 2), yf + mal / 3)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
    End If

    If Juego.Jugador.TantosDeFlor = 0 Or Juego.Tiradas > 0 Then
        If InStr(comandos, "c") > 0 Then Toggle comandos, "c"
        Line (dm + (mal / 3 * 2), yf + mal / 3 * 2)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
    End If

    If Juego.Voces <> "" Then
        bandera = 0
        For a = 1 To Len(Juego.Voces)
            c$ = Mid$(Juego.Voces, a, 1)
            If c$ <> "" And InStr(voces$, c$) > 0 And InStr(comandos, "q") = 0 Then
                bandera = 1
                If InStr(comandos, "q") = 0 Then Toggle comandos, "q"
                If InStr(comandos, "n") = 0 Then Toggle comandos, "n"
            End If
        Next
    End If

    If InStr(comandos, "q") = 0 And InStr(comandos, "n") = 0 Then
        Line (dm, yf)-Step(mal / 3 * 2, mal), _RGBA32(0, 0, 0, 220), BF
    End If

    If (InStr(Juego.Jugador.Palabra, "g") + InStr(Juego.Jugador.Palabra, "i") + InStr(Juego.Jugador.Palabra, "j")) > 0 Then
        If InStr(comandos, "g") > 0 Then Toggle comandos, "g"
        Line (dm + (mal / 3) * 2, yf)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
    End If
End Sub

' --------------------------------------------------------------------------------

Sub JugarPrimera ()
    Dim yf0, c, v, cIa, cJugador, i

    ' Si es mano el jugador, le pasa el control y espera, si no procesa su apertura
    If Juego.Mano = 0 Then

        CantosIA
        If Juego.TantosEl > Juego.Partida Then
            Juego.TantosEl = Juego.Partida
            Juego.Terminar = 6
            Exit Sub
        End If
        If Val(Mid$(Juego.IA.Peso, 7, 2)) < 8 And Juego.IA.TantosDeEnvido < 28 Then
            If Juego.Jugador.TantosDeFlor > 0 Then Juego.TantosYo = Juego.TantosYo + 3
            Juego.Terminar = 6
            Exit Sub
        End If
        yf0 = yf
        yf = yf2
        c = Val(Mid$(Juego.IA.Ordenado, 1, 1))
        Revelar c

        v = Val(Mid$(Juego.IA.Carta, c * 2 + 1, 2))
        Juego.cIA = Mid$(Juego.IA.Carta, c * 2 + 1, 2) + Mid$(Juego.IA.Peso, c * 2 + 1, 2)
        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + c
        yf = yf0
        JugarCartaIA c ' ahora turno del humano

        ActualizarMenu

        Asistente
        ProcesaEntrada
        If Juego.Terminar > 0 Then Exit Sub

        PrimeraJugador
        ' Juego.Turno = 0 - Turno de la IA
        ' Juego.Turno = 1 - Turno del jugador
        cIa = Val(Mid$(Juego.cIA, 3, 2))
        cJugador = Val(Mid$(Juego.cJugador, 3, 2))
        'Juego.Ttradas: Manos ganadas, decenas = IA, unidades = Jugador
        If cIa >= cJugador Then
            Juego.Turno = 0
            Juego.Tiradas = 10
        Else
            Juego.Turno = 1
            Juego.Tiradas = 1
        End If
    Else
        Asistente
        ProcesaEntrada
        If Juego.Terminar > 0 Then Exit Sub
        CantosIA
        If Juego.Terminar = 0 Then
            PrimeraJugador
            cJugador = Val(Mid$(Juego.cJugador, 3, 2))
            If Juego.Terminar > 0 Then Exit Sub

            c = MejorJugadaIA
            i = Val(Mid$(Trim(Str$(c)), 1, 1))
            yf0 = yf
            yf = yf2
            i = Val(Mid$(Juego.IA.Ordenado, i, 1))
            Revelar i

            v = Val(Mid$(Juego.IA.Carta, i * 2 + 1, 2))
            Juego.cIA = Mid$(Juego.IA.Carta, i * 2 + 1, 2) + Mid$(Juego.IA.Peso, i * 2 + 1, 2)
            JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + i

            i = Val(Mid$(Juego.IA.Ordenado, Floor(c / 10000), 1))
            JugarCartaIA i

            cIa = Val(Mid$(Juego.cIA, 3, 2))
            If cIa > cJugador Then
                Juego.Turno = 0
                Juego.Tiradas = 10
            Else
                Juego.Turno = 1
                Juego.Tiradas = 1
            End If
            yf = yf0
        End If
    End If
End Sub


' --------------------------------------------------------------------------------

Sub JugarSegunda ()
    Dim yf0, c, v, cia, cjugador, resto

    If Juego.Turno = 0 Then
        yf0 = yf
        yf = yf2
        c = Val(Mid$(Juego.IA.Ordenado, 1, 1))
        Revelar c
        v = Val(Mid$(Juego.IA.Carta, c * 2 + 1, 2))
        Juego.cIA = Mid$(Juego.IA.Carta, c * 2 + 1, 2) + Mid$(Juego.IA.Peso, c * 2 + 1, 2)
        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + c
        yf = yf0
        JugarCartaIA c ' ahora turno del humano
        ActualizarMenu
        Asistente
        ProcesaEntrada
        If Juego.Terminar > 0 Then Exit Sub
        PrimeraJugador
        ' Juego.Turno = 0 - Turno de la IA
        ' Juego.Turno = 1 - Turno del jugador
        cia = Val(Mid$(Juego.cIA, 3, 2))
        cjugador = Val(Mid$(Juego.cJugador, 3, 2))

        'Juego.Tiradas: Manos ganadas, decenas = IA, unidades = Jugador

        If cia >= cjugador Then
            If Juego.TantosEnJuego = 0 Then
                Juego.TantosEl = Juego.TantosEl + 1
            Else
                Juego.TantosEl = Juego.TantosEl + Juego.TantosEnJuego + Juego.EnJuego
                Juego.TantosEnJuego = 0
            End If
            Juego.Terminar = 2
        Else
            Juego.Turno = 1
            Juego.Tiradas = 11
        End If

    Else
        Asistente
        ProcesaEntrada
        If Juego.Terminar > 0 Then Exit Sub
        PrimeraJugador
        cjugador = Val(Mid$(Juego.cJugador, 3, 2))

        c = MejorJugadaIA
        resto = c Mod 100
        c = Floor(c / 10000)
        c = Val(Mid$(Juego.IA.Ordenado, c, 1))
        yf0 = yf
        yf = yf2
        Revelar c
        v = Val(Mid$(Juego.IA.Carta, c * 2 + 1, 2))
        Juego.cIA = Mid$(Juego.IA.Carta, c * 2 + 1, 2) + Mid$(Juego.IA.Peso, c * 2 + 1, 2)
        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + c
        yf = yf0
        JugarCartaIA c ' ahora turno del humano
        cia = Val(Mid$(Juego.cIA, 3, 2))
        If cia >= cjugador Then
            Juego.Turno = 0
            Juego.Tiradas = 11

        Else
            If Juego.TantosEnJuego = 0 Then
                Juego.TantosYo = Juego.TantosYo + 1
            Else
                Juego.TantosYo = Juego.TantosYo + Juego.TantosEnJuego + Juego.EnJuego
                Juego.TantosEnJuego = 0
            End If
            Juego.Terminar = 3
        End If
    End If
End Sub

' --------------------------------------------------------------------------------

Sub JugarTercera
    Dim yf0, c, v, cia, cjugador

    If Juego.Turno = 0 Then
        yf0 = yf
        yf = yf2
        c = Val(Mid$(Juego.IA.Ordenado, 1, 1))
        Revelar c
        v = Val(Mid$(Juego.IA.Carta, c * 2 + 1, 2))
        Juego.cIA = Mid$(Juego.IA.Carta, c * 2 + 1, 2) + Mid$(Juego.IA.Peso, c * 2 + 1, 2)
        cia = Val(Mid$(Juego.IA.Peso, c * 2 + 1, 2))
        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + c
        yf = yf0
        JugarCartaIA c ' ahora turno del humano
        ActualizarMenu
        Asistente
        ProcesaEntrada
        If Juego.Terminar = 0 Then
            cjugador = Val(Mid$(Juego.cJugador, 3, 2))
        End If
    Else
        Asistente
        ProcesaEntrada
        If Juego.Terminar > 0 Then Exit Sub
        PrimeraJugador
        cjugador = Val(Mid$(Juego.cJugador, 3, 2))

        yf0 = yf
        yf = yf2
        c = Val(Mid$(Juego.IA.Ordenado, 1, 1))
        Revelar c
        v = Val(Mid$(Juego.IA.Carta, c * 2 + 1, 2))
        Juego.cIA = Mid$(Juego.IA.Carta, c * 2 + 1, 2) + Mid$(Juego.IA.Peso, c * 2 + 1, 2)
        cia = Val(Mid$(Juego.IA.Peso, c * 2 + 1, 2))
        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 10 + c
        yf = yf0
        JugarCartaIA c ' ahora turno del humano
    End If

    If Juego.Terminar > 0 Then Exit Sub

    If cia > cjugador Then Juego.Tiradas = Juego.Tiradas + 10
    If cjugador > cia Then Juego.Tiradas = Juego.Tiradas + 1
    ' Si fue parda, de quien es el tanto?

    If Juego.Tiradas > 12 Then
        If Juego.TantosEnJuego = 0 Then
            Juego.TantosEl = Juego.TantosEl + 1
        Else
            Juego.TantosEl = Juego.TantosEl + Juego.TantosEnJuego + Juego.EnJuego
            Juego.TantosEnJuego = 0
        End If
        Juego.Terminar = 2
    Else If Juego.Tiradas > 1 Then
            If Juego.TantosEnJuego = 0 Then
                Juego.TantosYo = Juego.TantosYo + 1
            Else
                Juego.TantosYo = Juego.TantosYo + Juego.TantosEnJuego + Juego.EnJuego
                Juego.TantosEnJuego = 0
            End If
        End If
        Juego.Terminar = 3
    End If

End Sub

' --------------------------------------------------------------------------------

Sub PrepararMesa ()
    Dim p, p2, c, c2, v, v2, yf0, c$, d$, e$, f$, a
    For a = 0 To 2
        v = Baraja(iBaraja)
        v2 = Baraja(iBaraja + 3)

        p = Floor(v / 10) + 1
        p2 = Floor(v2 / 10) + 1
        c = v - ((p - 1) * 10) + 1
        c2 = v2 - ((p2 - 1) * 10) + 1
        c$ = LTrim$(Str$(v)): If Len(c$) < 2 Then c$ = "0" + c$
        e$ = LTrim$(Str$(v2)): If Len(e$) < 2 Then e$ = "0" + e$
        d$ = d$ + c$
        f$ = f$ + e$
        vx = ovx
        vy = ovy
        i = -(ovx / (grados / paso / 2))
        iBaraja = iBaraja + 1

        PreparaCarta
        DaCarta p, c, a

        PCopy _Display, 1
        yf0 = yf
        yf = yf2
        i = 0
        PreparaCarta
        DaCarta -1, 0, a
        PCopy _Display, 1
        yf = yf0
    Next
    Juego.Jugador.Carta = d$
    Juego.IA.Carta = f$
    Juego.Jugador.Peso = ""
    Juego.IA.Peso = ""
End Sub

' --------------------------------------------------------------------------------

Sub DebugMsg ()
    Dim a
    Color _RGB(255, 255, 255), 0
    _Font 16
    Locate 20, 1
    Print "Ponderacion(IA): ";
    For a = 0 To 2
        Print Mid$(Juego.IA.Peso, a * 2 + 1, 2); " ";
    Next
    Print
    Print "(media:"; Mid$(Juego.IA.Peso, 7); ")": ' Print "Tantos de envido:"; Juego.Jugador.TantosDeEnvido
    'Print "valoracion: "; Juego.IA.Valor; " {o}:"; Juego.IA.Ordenado
    Print

    Print "Ponderacion(J): ";
    For a = 0 To 2
        Print Mid$(Juego.Jugador.Peso, a * 2 + 1, 2); " ";
    Next
    Print
    Print "(media:"; Mid$(Juego.Jugador.Peso, 7); ")"
    'Print "valoracion: "; Juego.Jugador.Valor; " {o}:"; Juego.Jugador.Ordenado
    Print
End Sub

' --------------------------------------------------------------------------------

Sub PrepararMenu ()
    If Juego.Jugador.TantosDeFlor > 0 Then
        Toggle comandos, "t"
        Line (dm + (mal / 3 * 2), yf + mal / 3)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
    End If

    If Juego.Jugador.TantosDeEnvido > 0 Then
        Toggle comandos, "c"
        Line (dm + (mal / 3 * 2), yf + mal / 3 * 2)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
    End If

    NoQN comandos
End Sub

' --------------------------------------------------------------------------------

Sub Calculos ()
    SumaTantos Juego.Jugador
    SumaTantos Juego.IA
    Ponderar Juego.Jugador
    Ponderar Juego.IA
End Sub

' --------------------------------------------------------------------------------

Sub CargaVerso (Arch As String, Etiqueta As String)
    Dim FF, IniLOF, a, b, l$
    FF = FreeFile
    Arch = "db/" + Arch + ".txt"
    ' Abrir archivo en modo lectura
    Open Arch For Input As FF
    ' Tamanio del archivo
    IniLOF = LOF(FF)
    Line Input #FF, l$
    a = Int(Rnd * Val(l$) + 1)
    For b = 1 To a
        Line Input #FF, l$
    Next
    Close #FF

    Mensaje Etiqueta, l$
End Sub

' --------------------------------------------------------------------------------
Sub ProcesaEntrada ()
    Dim salida, x, y, s$, comando As String, mi, v, yf0

    yf0 = yf
    salida = 0
    While InKey$ <> "": Wend

    man = mal / 3
    Do
        _Limit 8
        ' Primero lee al dispositivo apuntador
        Do While _MouseInput
        Loop

        ' Si se hizo clic
        If _MouseButton(1) <> 0 Then
            ' Primero toma las coordenadas de ventana de donde se hizo clic
            x = _MouseX
            y = _MouseY

            If x <= (mal + dm) And x >= dm And y >= yf And y <= yf + mal Then
                x = Floor((x - dm) / man)
                If x = 2 Then
                    x = 1
                    y = Floor((y - yf) / man)
                Else
                    x = 0
                    y = Floor((y - yf) / (mal / 2))
                End If
                s$ = Trim(Str$(x)) + Trim(Str$(y))
                Select Case s$
                    Case Is = "00"
                        comando = "q"
                    Case Is = "01"
                        comando = "n"
                    Case Is = "10"
                        comando = "g"
                    Case Is = "11"
                        comando = "t"
                    Case Is = "12"
                        comando = "c"
                End Select

            Else
                If x >= tOfs And x <= (tOfs + an * 3) And y >= yf And y <= (yf + al) Then
                    x = Floor((x - tOfs) / an)
                    comando = Trim(Str$(x + 1))
                End If
                If usar_asistente = 1 Then
                    If x >= (Lado - tOfs) And y >= yf Then comando = "x"
                Else
                    If x <= (Lado - tOfs + man + man) And x >= (Lado - tOfs + man) And y >= (yf + man) And y <= (yf + man + man) Then comando = "m"
                End If
            End If
            ' Limpiar el buffer del dispositivo apuntador
            Do
                mi = _MouseInput
            Loop Until Not _MouseButton(1)

        End If

        a$ = InKey$
        If a$ <> "" Then comando = a$ ': Beep: Print comando; " "; comandos
        i = InStr(comandos, comando)
        If i = 0 Then comando = " "

        If comando <> "" Then

            Select Case comando
                Case Is = Chr$(27)
                    salida = 1
                Case Is = "q"
                    Print "Quiero"
                    If Len(Trim(comandos)) < 8 Then Juego.Voces = "q": salida = 2
                    If InStr(comandos, "q") > 0 Then NoQN comandos
                Case Is = "n"
                    Print "No quiero"
                    If Len(Trim(comandos)) < 8 Then
                        salida = 2
                        Juego.Voces = "n"
                    Else
                        Print Len(Trim(comandos)); comandos
                        _Display
                        a$ = Input$(1)
                    End If
                    If InStr(comandos, "q") > 0 Then NoQN comandos
                Case Is = "g"
                    '   g: grito
                    '       h: querido
                    '       i: retruco
                    '       j: vale 4
                    s$ = ""
                    If InStr(Juego.IA.Palabra, "g") > 0 Then s$ = "12"
                    If InStr(Juego.IA.Palabra, "i") > 0 Then s$ = "2"
                    If InStr(Juego.IA.Palabra, "g") = 0 Then s$ = "0"
                    MenuComando "g", "trv", s$

                    i = InStr("trv", Juego.Comando)
                    If i > 0 Then
                        Toggle comandos, "g"
                        Line (dm + (mal / 3) * 2, yf)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
                        Dim d$(1 To 3)
                        d$(1) = "g"
                        d$(2) = "i"
                        d$(3) = "j"
                        If i > 0 And InStr(Juego.Jugador.Palabra, d$(i)) = 0 Then Toggle Juego.Jugador.Palabra, d$(i)
                        IAResponderGrito
                        If Juego.Terminar > 0 Then salida = 2
                    End If
                Case Is = "t"
                    MenuComando "t", "efi5", "0123"
                    If Juego.Comando <> "" Then
                        Toggle comandos, "t"
                        Line (dm + (mal / 3 * 2), yf + mal / 3)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
                    End If
                Case Is = "c"
                    Juego.TantosYo = Juego.TantosYo + 3
                    If Juego.TantosYo >= Juego.Partida Then
                        Juego.TantosYo = Juego.Partida
                        Juego.Terminar = 5
                        salida = 2
                    Else
                        MenuComando "c", "fci5", "0123"
                        If Juego.Comando <> "" Then
                            Toggle comandos, "c"
                            Line (dm + (mal / 3 * 2), yf + mal / 3 * 2)-Step(mal / 3, mal / 3), _RGBA32(0, 0, 0, 220), BF
                        End If
                    End If
                    If InStr(Juego.Voces, "t") > 0 Then
                        Toggle Juego.Voces, "t"
                        If InStr(Juego.IA.Palabra, "t") > 0 Then Toggle Juego.IA.Palabra, "t"
                        If InStr(comandos, "q") > 0 Then NoQN comandos
                    End If
                    DibujaTantos
                Case Is = "1"
                    If Mid$(Juego.Jugador.Carta, 1, 1) <> "-" Then
                        v = Val(Mid$(Juego.Jugador.Carta, 1, 2))
                        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 0
                        Juego.cJugador = Mid$(Juego.Jugador.Carta, 1, 2) + Mid$(Juego.Jugador.Peso, 1, 2) + "0"
                        Juego.Jugador.Carta = "--" + Mid$(Juego.Jugador.Carta, 3)
                    End If
                Case Is = "2"
                    If Mid$(Juego.Jugador.Carta, 3, 1) <> "-" Then
                        v = Val(Mid$(Juego.Jugador.Carta, 3, 2))
                        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 1
                        Juego.cJugador = Mid$(Juego.Jugador.Carta, 3, 2) + Mid$(Juego.Jugador.Peso, 3, 2) + "1"
                        Juego.Jugador.Carta = Mid$(Juego.Jugador.Carta, 1, 2) + "--" + Mid$(Juego.Jugador.Carta, 5)
                    End If
                Case Is = "3"
                    If Mid$(Juego.Jugador.Carta, 5, 1) <> "-" Then
                        v = Val(Mid$(Juego.Jugador.Carta, 5, 2))
                        JuegaCarta ValorPalo(v) + 1, ValorCarta(v) + 1, 2
                        Juego.cJugador = Mid$(Juego.Jugador.Carta, 5, 2) + Mid$(Juego.Jugador.Peso, 5, 2) + "2"
                        Juego.Jugador.Carta = Mid$(Juego.Jugador.Carta, 1, 4) + "--"
                    End If
                Case Is = "m"
                    Juego.Terminar = 5
                    salida = 2
                    PCopy 6, _Display
                Case Is = "x"
                    If usar_asistente = 1 Then
                        usar_asistente = 0
                        'PCopy 6, _Display
                        _PutImage (Lado - tOfs, yf0), bufAsist
                        Print "Cerrar asistente"
                        _Display
                    End If

                Case Is = "r"
                    If _SndPlaying(bgm) <> 0 Then _SndStop bgm
                    Run
            End Select

            centroXi = Lado \ 2: centroYi = centroXi
            If InStr("123", comando) > 0 Then
                salida = 2
                Toggle comandos, comando
                If usar_asistente = 1 Then
                    usar_asistente = 0
                    _PutImage (Lado - tOfs, yf0), bufAsist
                End If

                'Sleep 1
            End If

            comando = ""

        End If

        _Display
    Loop Until salida <> 0
    If salida = 1 Then Juego.Terminar = 1
End Sub

' --------------------------------------------------------------------------------

Sub LimpiarCentro
    Sleep 1
    centroXi = Lado \ 2
    centroYi = centroXi

    Line (centroXi - an, centroYi - al / 1.5)-Step(an * 2, al * 1.2), VERDE_OSCURO, BF
End Sub

' --------------------------------------------------------------------------------

Sub MenuComando (mcomando As String, Teclas As String, Habilitados As String)
    Dim an, al, r, x, y, f As Long, Botones(4) As String, Titulo As String, i, m As Long, ofs, Tecla As String, l, salida, opcion As String, oTeclas As String

    oTeclas = Teclas
    an = 590 ' Ancho
    al = 95 ' Alto
    r = an / al ' Relacion de aspecto
    l = Len(Teclas)
    m = _NewImage(an, al, 32)
    ofs = Lado * .05
    f = _LoadFont("Font/nk57-monospace-se-sb.otf", 12)
    PCopy _Display, 3
    _Dest m
    Line (0, 0)-Step(an, al), _RGB(0, 0, 32), BF
    Select Case mcomando
        Case Is = "t"
            Titulo = "TOQUES"
            Botones(0) = "(E)nvido"
            Botones(1) = "(F)alta"
            Botones(2) = "(I)gualar"
            Botones(3) = "(5) tantos"

        Case Is = "c"
            Titulo = "CANTO"
            Botones(0) = "(F)lor"
            Botones(1) = "(C)ontraflor"
            Botones(2) = "(I)gualar"
            Botones(3) = "(5) tantos"

        Case Is = "g"
            Titulo = "GRITOS"
            Botones(0) = "(T)ruco"
            Botones(1) = "(R)etruco"
            Botones(2) = "(V)ale 4"
            Botones(3) = ""
    End Select

    _Font f
    Color BLANCO, 0
    Locate 1, 560
    Print "[X]"
    Locate 2, 25
    Titulo = " " + Titulo + " "
    i = (60 - Len(Titulo)) / 2
    If i <> Floor(i) Then i = Floor(i) + 1
    Titulo = String$(i, "-") + Titulo + String$(i, "-")
    Titulo = Mid$(Titulo, 1, 60)
    Print Titulo
    For i = 0 To 3
        Line (25 + i * 138, 38)-Step(122, 32), BLANCO, B
        Locate 5, 32 + i * 138
        Print Botones(i);
        If InStr(Habilitados, Trim(Str$(i))) < 1 And (i + 1) Then
            Line (25 + i * 138, 38)-Step(122, 32), _RGBA(0, 0, 0, 164), BF
            If i < l Then
                Tecla = Mid$(oTeclas, i + 1, 1)
                Toggle Teclas, Tecla
            End If
        End If
    Next

    _Dest 0
    Line (0, 0)-Step(Lado, Lado), _RGBA(0, 0, 0, 128), BF
    an = Lado * .90
    al = an / r
    _PutImage (ofs, Lado - al - ofs)-Step(an, al), m
    _Display

    If Len(oTeclas) < 4 Then oTeclas = oTeclas + String$(4 - Len(Teclas), " ")
    Juego.Comando = ""
    opcion = ""
    Teclas = Teclas + "x" + Chr$(27)
    oTeclas = oTeclas + "x" + Chr$(27)
    salida = 0
    While salida = 0
        _Limit 8
        ' Primero lee al dispositivo apuntador
        Do While _MouseInput
        Loop

        ' Si se hizo clic
        If _MouseButton(1) <> 0 Then
            ' Primero toma las coordenadas de ventana de donde se hizo clic
            x = _MouseX
            y = _MouseY

            If x >= ofs And x <= (ofs + an) And y > (Lado - al - ofs) And y <= (Lado - ofs) Then
                x = Floor((x - ofs) * 590 / an)
                y = Floor((y - (Lado - al - ofs)) * 95 / al)

                If x < 591 And x > 550 And y > 0 And y < 12 Then opcion = "x"
                If y > 37 And y < 61 Then
                    For i = 0 To 3
                        If x >= (25 + i * 138) And x <= (i * 138 + 147) Then opcion = Mid$(oTeclas, i + 1, 1) ': Print "i,t"; i; oTeclas: _Display
                    Next
                End If
            Else
                opcion = "x"
            End If
            'Print "> "; opcion, "["; Teclas; "]"; , InStr(Teclas, opcion): _Display: a$ = Input$(1)
            If InStr(Teclas, opcion) < 1 Then opcion = ""
            Do
                i = _MouseInput
            Loop Until Not _MouseButton(1)

        End If
        a$ = InKey$
        If a$ <> "" Then opcion = a$
        If opcion <> "" Then
            If (opcion = "x" Or opcion = Chr$(27)) Then
                salida = 1
                Juego.Comando = "x"
            Else
                i = InStr(Teclas, opcion)
                If i > 0 Then Juego.Comando = opcion: salida = 1
            End If
        End If
    Wend

    PCopy 3, _Display
End Sub

' --------------------------------------------------------------------------------

Sub IAResponderGrito ()
    Dim enMano, enMesa, Media, i, j, k, tmp As String, v$

    ' g: grito
    '   h: quiero
    '   i: retruco
    '   j: vale 4
    v$ = "ghij"
    enMano = 0
    enMesa = 0
    Media = 0
    tmp = Trim(Juego.IA.Ordenado)
    If Len(tmp) > 0 Then
        For i = 1 To Len(tmp)
            j = Val(Mid$(Juego.IA.Ordenado, i, 1))
            enMano = enMano + Val(Mid$(Juego.IA.Peso, (j * 2) + 1, 2))
        Next
        Media = Floor(enMano / Len(tmp))
    End If
    ' Valores posibles para Juego.Terminar
    ' 0:Mo terminar
    ' 1: terminar la partida
    ' 2:Tirada ganada por IA
    ' 3: Tirada ganada por jugador
    ' 4: Alguien gano el partido
    ' 5: IA Gana mano
    ' 6: Jugador gana mano

    Print "Me queres correr? ";
    ' Me queres correr?
    If Trim(Juego.cIA) <> "" Then
        j = 0
        enMesa = Val(Mid$(Juego.cIA, 3, 2))
        If Juego.Tiradas > 9 And Media > 8 Then ' Gane la primera y tengo una buena mano
            j = 1
        End If
        If j = 0 Then ' No gan�e la primera o tengo una mala mano
            Select Case Media
                Case Is > 17
                    j = j + 1000 ' Tengo al menos el cuatro de la muestra, mano ganadora segura
                Case Is > 15
                    j = j + 100 ' Buenas chances en la mano
                Case Is > 10
                    j = j + 10 ' Aceptable, apenas
                Case Else
                    If Media < 9 And enMesa < 9 Then ' Mala mano, jugue una carta baja
                        Print "no quiero"; Media; enMesa
                        Juego.Terminar = 3
                        Exit Sub
                    Else ' Mala mano, jugue una buena carta
                        Print "quiero, voy a la tercera"
                        j = j + 10000
                    End If
            End Select
        End If
        k = 0
        For i = 1 To Len(v$)
            If InStr(Juego.Jugador.Palabra, Mid$(v$, i, 1)) > 0 And k = 0 Then k = InStr(Juego.Jugador.Palabra, Mid$(v$, i, 1))
        Next
        Print "O jugue una buena o tengo buena mano "; j
        If j > 0 Then ' Segun valoracion, tengo algo quue valga la pena?
            Print "j >"; j; "["; Mid$(Juego.Jugador.Palabra, k, 1); "] k >"; k; "tej > "; Juego.TantosEnJuego

            If k > 0 Then
                Print "Respondo a tu ";

                Select Case k
                    Case Is = 1
                        Toggle Juego.Jugador.Palabra, "g"
                        If InStr(Juego.Voces, "g") > 0 Then Toggle Juego.Voces, "g"
                        Print "truco"
                    Case Is = 2
                    Case Is = 3
                        Toggle Juego.Jugador.Palabra, "i"
                        If InStr(Juego.Voces, "g") > 0 Then Toggle Juego.Voces, "i"
                        Print "retruco"
                    Case Is = 4
                        Toggle Juego.Jugador.Palabra, "j"
                        If InStr(Juego.Voces, "g") > 0 Then Toggle Juego.Voces, "j"
                        Print "vale 4"
                End Select
            End If

            Juego.TantosEnJuego = Juego.TantosEnJuego + 1
            Print "v> ["; Juego.Voces; "]"
            If k < 4 Then ' Dijiste truco o retruco
                If j > 900 And j < 9000 Then Print "Quiero, vale 4"
                Select Case k
                    Case Is = 3 ' Retruco
                        If j > 900 And j < 9000 Then
                            Print "Quiero, vale 4": Juego.EnJuego = 4
                            Toggle Juego.Voces, "j"
                            Toggle Juego.IA.Palabra, "j"
                        Else If j > 90 Then
                                Print "Quiero"
                                Juego.EnJuego = Juego.EnJuego + 1
                                Toggle Juego.Voces, "h"
                                Toggle Juego.IA.Palabra, "h"
                            End If
                        End If
                    Case Is = 1 ' Truco
                        If j > 900 And j < 9000 Then
                            Print "Quiero, vale 4"
                            Toggle Juego.Voces, "j"
                            Toggle Juego.IA.Palabra, "j"
                        Else If j > 90 Then
                                Print "Quiero, retruco"
                                Juego.EnJuego = Juego.EnJuego + 1
                                Toggle Juego.Voces, "i"
                                Toggle Juego.IA.Palabra, "i"
                            Else If j = 1 Then
                                    Print "Gane la primera, tengo una buena mano, quiero"
                                    Juego.EnJuego = Juego.EnJuego + 1
                                    Toggle Juego.Voces, "h"
                                    Toggle Juego.IA.Palabra, "h"
                                Else
                                    If j > 9 Then
                                        Print "Quiero"
                                        Juego.EnJuego = Juego.EnJuego + 1
                                        Toggle Juego.Voces, "h"
                                        Toggle Juego.IA.Palabra, "h"
                                    Else Print "No, esta vez no quiero "; j: Juego.Terminar = 3
                                    End If
                                End If
                            End If
                        End If
                End Select
            Else ' Dijiste vale 4
                If j > 900 And j < 9000 Then
                    Print "Quiero!"
                    Toggle Juego.Voces, "h"
                    Toggle Juego.IA.Palabra, "h"
                Else ' Este es el lugar donde ver si me mienten o debo mentir
                    Print "No, mejor no esta vez"
                End If
            End If

            'If k = 1 Then
            '    Toggle Juego.Jugador.Palabra, "g"
            '    If InStr(Juego.Voces, "g") > 0 Then Toggle Juego.Voces, "g"
            '    Select Case j
            '        Case Is > 900
            '            Toggle Juego.IA.Palabra, "j" ' vale 4
            '            Toggle Juego.Voces, "j"
            '        Case Is > 90
            '            Toggle Juego.IA.Palabra, "i" ' retruco
            '            Toggle Juego.Voces, "i"
            '        Case Else
            '            Toggle Juego.IA.Palabra, "h" ' quiero
            '            Toggle Juego.Voces, "h"
            '    End Select
            '    Juego.TantosEnJuego = Juego.TantosEnJuego + 1
            'End If
            _Display
            a$ = Input$(1)
        End If
    Else
        If Media > 7 Then
            Print "Quiero"
            If InStr(Juego.Jugador.Palabra, "g") > 0 Then Juego.EnJuego = 2
            If InStr(Juego.Jugador.Palabra, "i") > 0 Then Juego.EnJuego = 3
            If InStr(Juego.Jugador.Palabra, "j") > 0 Then Juego.EnJuego = 4
            Toggle Juego.Voces, "h"
            Toggle Juego.IA.Palabra, "h"
        Else
            'Juego.TantosYo = Juego.TantosYo + Juego.EnJuego
            'Juego.EnJuego = 0
            Juego.Terminar = 6
            Print "No quiero"
        End If
    End If

    'Print Juego.IA.Peso; " ["; Juego.IA.Ordenado; "] ["; Trim(Juego.cIA); "]"
    'Print "Valoracion. Mano:"; enMano; "(media "; Trim(Str$(Media)); ") Mesa:"; enMesa

End Sub

' --------------------------------------------------------------------------------

Sub DepurarVoces
    ' t: toque
    '   u: envido
    '   v: hasta igualar
    '   w: falta envido
    Dim Toques$, Voz$, i
    Toques$ = "tuvw"
    For i = 1 To Len(Toques$)
        Voz$ = Mid$(Toques$, i, 1)
        If InStr(Juego.Voces, Voz$) > 0 Then
            Toggle Juego.Voces, Voz$
        End If
    Next

End Sub

' --------------------------------------------------------------------------------
' -- Cargar archivos externos
' --------------------------------------------------------------------------------

' $Include:'Lib\INIFile.bm' ' Leer / modificar archivos de configuracion

