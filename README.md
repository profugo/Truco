# Truco Uruguayo

<a name="truco-uruguayo"></a>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Licencia GPL][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="/profugo/Truco">
    <img src="/profugo/Truco/raw/branch/main/img/china.png" alt="Logo" width="160" height="120">
  </a>

  <h3 align="center">Truco Uruguayo</h3>

  <p align="center">
    Versión uruguaya del truco con muestra
    <br />
    <a href="/profugo/Truco"><strong>Explorar los archivos</strong></a>
    <br />
    <br />
    <a href="/profugo/Truco/issues">Reportar un Bug</a>
    ·
    <a href="/profugo/Truco/issues">Pedir Característica</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Tabla de contenidos</summary>
  <ol>
    <li>
      <a href="#acerca-del-proyecto">Acerca del proyecto</a>
      <ul>
        <li><a href="#hecho-con">Hecho con</a></li>
      </ul>
    </li>
    <li>
      <a href="#comenzando">Comenzando</a>
      <ul>
        <li><a href="#prerequisitos">Prerequisitos</a></li>
        <li><a href="#installation">Instalación</a></li>
      </ul>
    </li>
    <li><a href="#uso">Uso</a></li>
    <li><a href="#hoja-de-ruta">Hoja de ruta</a></li>
    <li><a href="#licencia">Licencia</a></li>
    <li><a href="#contacto">Contacto</a></li>
    <li><a href="#reconocimientos">Reconocimientos</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## Acerca del proyecto

<div align="center">
  <a href="/profugo/Truco">
<img src="/profugo/Truco/raw/branch/main/img/captura.png" alt="Captura">
  </a>
</div>

Juegos de cartas hay un montón. De Truco, unos cuantos, gratis, pero no libres y no de truco uruguayo.<br>
Lógica de programador linuxero, si no existe se hace y si se hace, se comparte.<br>
Con esto en mente, puse manos a la obra y retomando un viejo proyecto en pascal empezado allá por el 2004, lo convertí a QBasic y lo estoy publicando acá (si es que no lo terminé para cuando leas esto)

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



### Hecho con

Este es un desarollo hecho en QBasic usando el genial QB64 Phoenix Edition porque usau un lenguaje con manejo de objetos lo hace demasiado fácil :-P

* [QB64-url]

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- GETTING STARTED -->
## Comenzando

Como todo proyecto, en el final finaliza y empieza por adelante

### Prerequisitos

Es una aplicación gráfica compilada con g++, depende de GLU para los gráficos y QB64 necesita a glibc.

* glut
* glibc

### Instalación

No hay mucho que hacer para instalar, basta clonar el repo o descargarlo

1. Clonar el repo
   ```sh
   git clone https://codeberg.org/profugo/Truco.git
   ```

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- USAGE EXAMPLES -->
## Uso

No hay mucha magia acá, siempre podés vichar el [Manual](/profugo/Truco/src/branch/main/Manual)

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- ROADMAP -->
## Hoja de ruta

- [ ] Gritos *
    - [ ] Truco
    - [ ] Retruco
    - [ ] Vale 4
- [ ] Cantos
    - [ ] Flor
    - [ ] Contraflor
    - [ ] Hasta igualar
    - [ ] 5 tantos
- [ ] Toques
    - [ ] Envido
    - [ ] Falta envido
    - [ ] Hasta igualar
    - [ ] 5 tantos

* Por ahora solo acepta grito en primera ronda

Mirá la [lista](/profugo/Truco/issues) para ver las propuestas (y problemas cococidos).

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>


<!-- LICENSE -->
## Licencia

Este programa se distribuye con licencia GPL V3. Ver [LICENCIA](LICENSE).

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- CONTACT -->
## Contacto

Guillermo Portales - [@darkasm](https://t.me/) - darknoid@yahoo.com

Project Link: [https://codeberg.org/profugo/Truco](https://codeberg.org/profugo/Truco)

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Reconocimientos

Nada de esto habría sido posible sin pararme a ver el mundo sobre hombros de gigantes, estos son los que me vienen a la cabeza ahora

* [Choose an Open Source License](https://choosealicense.com)
* [Tutorial QB64 PE](https://www.qb64tutorial.com/)
* [Juan Eguia Abad](https://www.youtube.com/@siempreretro)
* [Truco Arbiser](https://www-2.dc.uba.ar/charlas/lud/truco/)

<p align="right">(<a href="#truco-uruguayo">volver</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: /profugo/Truco/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: /profugo/Truco/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: /profugo/Truco/stargazers
[issues-url]: /profugo/Truco/issues
[license-shield]: https://img.shields.io/badge/Licencia-GPL-green
[license-url]: /profugo/Truco/src/branch/main/LICENSE
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png
[QB64-url]: https://qb64phoenix.com
[Svelte.dev]: https://img.shields.io/badge/Svelte-4A4A55?style=for-the-badge&logo=svelte&logoColor=FF3E00
[Svelte-url]: https://svelte.dev/
[Laravel.com]: https://img.shields.io/badge/Laravel-FF2D20?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com
[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[JQuery.com]: https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white
[JQuery-url]: https://jquery.com 
