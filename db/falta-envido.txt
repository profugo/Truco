2
Una vez una paloma;ofrecio darme su nido,;y yo creyendo una broma;no le eche la Falta envido.
No se ponga tan contento;por el envite que ha echao;porque escuchara al momento:;Falta envido, cuniao.

 "Falta envido": [
    "Pido gancho\ngancho pido\nChupate esta\nFalta Envido !",
    "No todo est\u00e1 perdidocontest\u00e1 esta\nFalta Envido",
    "Si queres ganarme el\npartido,\nquereme esta\nfalta envido",
    "A este rival\ncomedido\nlo echo con\nFalta Envido",
    "A este neurotico\ncompungido\nlo corro con\nla falta envido",
    "Sos un adversario\ndormido.\nNo importa:\nyo te despierto\ncon un falta envido",
    "No creas que\nme has vencido:\nAtajate esta\nFalta Envido",
    "A que te achicas:\nFalta Envido",
    "Para el tanto yo no\ntengo nada pero\nvos menos... asi que\nFalta Envido",
    "Yo,el computador digoAgarrate fuerte que\nte mando \nla Falta Envido !",
    "Che, humano\nno sea tan altivo\nque yo lo desafio\ncon una falta envido"
  ],
